package trace_validation.requirement_converter;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import trace_validation.requirement_converter.interfaces.nba.*;

/**
 * Unit test for requirement converter
 */
public class AppIT {
	static final int EXPECTED_NR_OF_STATES_NBA = 4;
	static final int EXPECTED_NR_OF_TRANSITIONS = 40;
	static final int EXPECTED_NR_OF_ACCEPTING_STATES = 4;
	static final int EXPECTED_NR_OF_INITIAL_STATES = 4;
	static final int EXPECTED_NR_OF_WHERE = 3;

	@Rule
	public TemporaryFolder folder = new TemporaryFolder();

	/**
	 * Integration (functional) test
	 * Tests the properties of the positive automaton, whether they have changed
	 * compared to the 'original' implementation.
	 */
	@Test
	public void testProperties() {
		// relative path to pom.xml
		final String tracePath = "src/main/resources/ltl3_simple.json";
		File outFile = null;
		File negOutFile = null;
		try {
			outFile = folder.newFile("ltl3_simple.nba.json");
			negOutFile = folder.newFile("neg_ltl3_simple.nba.json");
		} catch (IOException e) {
			e.printStackTrace();
		}

		// run the SUT
		final String outPath = outFile.getAbsolutePath();
		final String negOutPath = negOutFile.getAbsolutePath();
		final String[] args = { tracePath, outPath, negOutPath };
		Converter.main(args);

		// get the SUT result
		Nba nbaRequirement = null; 
		try { 
			String nbaString = readFile(outPath);
			System.out.println(nbaString); 
			nbaRequirement = NbaParser.fromJsonString(nbaString); 
		} catch (IOException e) { 
			e.printStackTrace(); 
		}

		int nrNbaStates = nbaRequirement.getAutomaton().getStates().size();
		int nrTransitions = nbaRequirement.getAutomaton().getTransitions().size();
		int nrWhere = nbaRequirement.getWhere().size();
		int nrAccepting = 0;
		int nrInitial = 0;
		
		for (State state : nbaRequirement.getAutomaton().getStates()) {
			if (state.getAccepting()) nrAccepting++;
			if ((boolean) state.getInitial()) nrInitial++;
		}
		
		assertTrue("Wrong number of NBA states generated", EXPECTED_NR_OF_STATES_NBA == nrNbaStates);
		assertTrue("Wrong number of NBA transitions generated", EXPECTED_NR_OF_TRANSITIONS == nrTransitions);
		assertTrue("Wrong number of initial NBA states generated", EXPECTED_NR_OF_INITIAL_STATES == nrInitial);
		assertTrue("Wrong number of accepting NBA states generated", EXPECTED_NR_OF_ACCEPTING_STATES == nrAccepting);
		assertTrue("Wrong number of WHEREs copied from requirement", EXPECTED_NR_OF_WHERE == nrWhere);
	}

	/**
	 * Tests if the output equals the expected output.
	 */
	@Test
	public void testOutput() {
		// relative path to pom.xml
		final String tracePath = "src/main/resources/ltl3_simple.json";
		File outFile = null;
		File negOutFile = null;
		try {
			outFile = folder.newFile("ltl3_simple.nba.json");
			negOutFile = folder.newFile("neg_ltl3_simple.nba.json");
		} catch (IOException e) {
			e.printStackTrace();
		}

		// run the SUT
		final String outPath = outFile.getAbsolutePath();
		final String negOutPath = negOutFile.getAbsolutePath();
		final String[] args = { tracePath, outPath, negOutPath };
		Converter.main(args);

		// Read result
		final String originalNbaString = 
				"{\"automaton\":{\"states\":[{\"accepting\":true,\"ID\":\"N3_0"
				+ "\",\"initial\":true},{\"accepting\":true,\"ID\":\"init_0\","
				+ "\"initial\":true},{\"accepting\":true,\"ID\":\"N9_0\",\"init"
				+ "ial\":true},{\"accepting\":true,\"ID\":\"N12_0\",\"initial\""
				+ ":true}],\"transitions\":[[\"N3_0\",[\"a\"],\"N3_0\"],[\"init"
				+ "_0\",[\"b\"],\"N9_0\"],[\"N12_0\",[\"a\",\"b\",\"c\"],\"N3_0"
				+ "\"],[\"init_0\",[\"c\"],\"N12_0\"],[\"N3_0\",[\"b\",\"c\"],"
				+ "\"N9_0\"],[\"N12_0\",[\"a\",\"c\"],\"N3_0\"],[\"init_0\",[\""
				+ "a\",\"b\",\"c\"],\"N12_0\"],[\"N9_0\",[\"a\",\"b\",\"c\"],\""
				+ "N12_0\"],[\"N12_0\",[\"a\",\"b\"],\"N3_0\"],[\"N9_0\",[\"a\""
				+ ",\"b\",\"c\"],\"N3_0\"],[\"N3_0\",[\"a\",\"b\",\"c\"],\"N9_0"
				+ "\"],[\"init_0\",[\"b\",\"c\"],\"N9_0\"],[\"N9_0\",[\"a\",\"c"
				+ "\"],\"N12_0\"],[\"N9_0\",[\"a\",\"b\"],\"N3_0\"],[\"init_0\""
				+ ",[\"a\",\"b\",\"c\"],\"N9_0\"],[\"N9_0\",[\"a\",\"b\",\"c\"]"
				+ ",\"N9_0\"],[\"N9_0\",[\"b\",\"c\"],\"N9_0\"],[\"init_0\",[\""
				+ "a\",\"b\",\"c\"],\"N3_0\"],[\"N9_0\",[\"b\"],\"N9_0\"],[\"N1"
				+ "2_0\",[\"a\"],\"N3_0\"],[\"init_0\",[\"a\",\"b\"],\"N3_0\"],"
				+ "[\"N3_0\",[\"a\",\"c\"],\"N3_0\"],[\"N3_0\",[\"b\"],\"N9_0\""
				+ "],[\"N3_0\",[\"a\",\"b\",\"c\"],\"N3_0\"],[\"init_0\",[\"a\""
				+ ",\"c\"],\"N3_0\"],[\"init_0\",[\"a\"],\"N3_0\"],[\"init_0\","
				+ "[\"a\",\"c\"],\"N12_0\"],[\"N9_0\",[\"b\",\"c\"],\"N12_0\"],"
				+ "[\"N9_0\",[\"a\",\"b\"],\"N9_0\"],[\"N3_0\",[\"a\",\"c\"],\""
				+ "N12_0\"],[\"N3_0\",[\"a\",\"b\"],\"N9_0\"],[\"N3_0\",[\"a\","
				+ "\"b\",\"c\"],\"N12_0\"],[\"N9_0\",[\"a\"],\"N3_0\"],[\"N9_0"
				+ "\",[\"c\"],\"N12_0\"],[\"N9_0\",[\"a\",\"c\"],\"N3_0\"],[\"N"
				+ "3_0\",[\"b\",\"c\"],\"N12_0\"],[\"init_0\",[\"b\",\"c\"],\"N"
				+ "12_0\"],[\"N3_0\",[\"c\"],\"N12_0\"],[\"N3_0\",[\"a\",\"b\"]"
				+ ",\"N3_0\"],[\"init_0\",[\"a\",\"b\"],\"N9_0\"]]},\"name\":\""
				+ "Simple requirement example\",\"split\":null,\"where\":[{\"ev"
				+ "ent\":\"a\",\"off\":\"hw:barectf_ust_a\",\"off parameters\":"
				+ "[null,\"finish\"],\"on\":\"hw:barectf_ust_a\",\"on parameter"
				+ "s\":[null,\"start\"]},{\"event\":\"b\",\"off\":\"hw:barectf_"
				+ "off_event\",\"off parameters\":[\"1\"],\"on\":\"hw:barectf_o"
				+ "n_event\",\"on parameters\":[\"1\"]},{\"event\":\"c\",\"off"
				+ "\":\"hw:c_off\",\"off parameters\":null,\"on\":\"hw:c_on\","
				+ "\"on parameters\":null}]}";
		String outputNbaString = "";
		Nba originalNba = null;
		Nba outputNba = null;
		try {
			originalNba = NbaParser.fromJsonString(originalNbaString); 
			outputNbaString = readFile(outPath);
			outputNba = NbaParser.fromJsonString(outputNbaString);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		System.out.println(outputNba);
		assertTrue("NBA output did not equal expected output", 
				originalNba.equals(outputNba));
	}
	
	private static String readFile(String file) throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader(file));
		String line = null;
		StringBuilder stringBuilder = new StringBuilder();
		String ls = System.getProperty("line.separator");

		try {
			while ((line = reader.readLine()) != null) {
				stringBuilder.append(line);
				stringBuilder.append(ls);
			}

			return stringBuilder.toString();
		} finally {
			reader.close();
		}
	}
}
