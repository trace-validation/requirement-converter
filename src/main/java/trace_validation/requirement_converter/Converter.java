package trace_validation.requirement_converter;

import java.io.*;
import java.util.*;

import trace_validation.requirement_converter.interfaces.ltl.*;
import trace_validation.requirement_converter.interfaces.nba.*;
import trace_validation.requirement_converter.ltl2nba.*;

public class Converter {
	
	public static void main(String args[]) {
		String jsonString;
		Requirement data = null;
		
		if (args.length != 3) {
			System.out.println("Error, incorrect number of arguments.");
			System.out.println("Argument 1: LTL JSON file to read");
			System.out.println("Argument 2: Positive NBA JSON file to write");
			System.out.println("Argument 3: Negated NBA JSON file to write");
			return;
		}
		
		try {			
			jsonString = readFile(args[0]);
			data = RequirementParser.fromJsonString(jsonString);
		} catch(Exception e) {
			System.err.println(e);
		}
		System.out.println("Parsed LTL");
		
		LtlSimplifier simplifier = new LtlSimplifier();
		Operator simplifiedFormula = simplifier.simplify(data.getLTL().getFormalLTL()[0]);
		System.out.println("Simplified LTL+");

		Operator negFormula = new Operator(); // Add Negation
		negFormula.setNot(data.getLTL().getFormalLTL());
		Operator negSimplifiedFormula = simplifier.simplify(negFormula);
		System.out.println("Simplified LTL-");
		
		TableauCreator tc = new TableauCreator();
		Set<TableauNode> tableauNodes = tc.createGraph(simplifiedFormula);
		System.out.println("Constructed Tableau+");
		
		Set<TableauNode> negTableauNodes = tc.createGraph(negSimplifiedFormula);
		System.out.println("Constructed Tableau-");		
		
		// Tableau -> LGBA -> GBA -> NBA
		NBA internalNBA = new NBA(new GBA(new LGBA(tableauNodes, simplifiedFormula)));
		System.out.println("Generated NBA+");
		
		NBA negInternalNBA = new NBA(new GBA(new LGBA(negTableauNodes, negSimplifiedFormula)));
		System.out.println("Generated NBA-");
		
		Nba nba = internalNBA.toNBAInterface(data);
		Nba negNba = negInternalNBA.toNBAInterface(data);
		
		// Write final result to file
		try {
			String automaton = NbaParser.toJsonString(nba);
			writeFile(args[1], automaton);
			automaton = NbaParser.toJsonString(negNba);
			writeFile(args[2], automaton);
		} catch(Exception e) {
			System.out.println(e);
		}
		
		System.out.println("Done. Result in " + args[1] + " and " + args[2] + ".");
	}
	
	private static String readFile(String file) throws IOException {
	    BufferedReader reader = new BufferedReader(new FileReader (file));
	    String         line = null;
	    StringBuilder  stringBuilder = new StringBuilder();
	    String         ls = System.getProperty("line.separator");

	    try {
	        while((line = reader.readLine()) != null) {
	            stringBuilder.append(line);
	            stringBuilder.append(ls);
	        }

	        return stringBuilder.toString();
	    } finally {
	        reader.close();
	    }
	}
	
	private static void writeFile(String file, String content) throws IOException {
		BufferedWriter writer = new BufferedWriter(new FileWriter(file));
	    writer.write(content);
		writer.close();
	}
}
