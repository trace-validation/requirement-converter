package trace_validation.requirement_converter.ltl2nba;

import java.util.*;

import trace_validation.requirement_converter.interfaces.ltl.Operator;

public class TableauCreator {

	private int nodeCounter = 0;
	
	public TableauCreator() {}
	
	public Set<TableauNode> createGraph(Operator formula) {
		TableauNode root = new TableauNode();
		TableauNode init = new TableauNode();
		init.setName("init");
		
		/* Set the dummy edge to 'init' */
		Set<Operator> newProperties = new HashSet<>();
		newProperties.add(formula);
		root.setName("RT");
		root.getIncoming().add(init);
		root.setNewProperties(newProperties);
		root.setOldProperties(new HashSet<Operator>());
		root.setNextProperties(new HashSet<Operator>());
		
		Set<TableauNode> nodesSet = new HashSet<TableauNode>();
		
		Set<TableauNode> result = expand(root, nodesSet);
		result.add(init);
		return result;
	}
	
	private Set<TableauNode> expand(TableauNode node, Set<TableauNode> nodesSet) {
		TableauNode outNode = new TableauNode(node);
		Set<TableauNode> outNodesSet = new HashSet<>(nodesSet);
		
		/* Line 4 */
		if (node.getNewProperties().isEmpty()) {
			/* Line 5 */
			TableauNode tn = checkNodeSetExistence(node, outNodesSet);
			if (tn != null) {
				// Add incoming edges of node to tn
				/* Line 6 */
				tn.getIncoming().addAll(node.getIncoming());
				/* Line 7 */
				return outNodesSet;
			} else { /* No similar node in nodesSet */
				/* Line 8-10 */
				// Add node
				outNodesSet.add(node);
				 // Create new node
				outNode.setName(newNodeName());
				Set<TableauNode> newIncoming = new HashSet<>();
				newIncoming.add(node);
				outNode.setIncoming(newIncoming);
				outNode.setNewProperties(node.getNextProperties());
				outNode.setOldProperties(new HashSet<Operator>());
				outNode.setNextProperties(new HashSet<Operator>());
				return expand(outNode, outNodesSet);
			}
		/* Line 11 */
		} else { /* New is not empty */
			/* Line 12 */
			Operator newFormula = null;
			for (Operator prop : node.getNewProperties()) {
				// Select the very first formula
				newFormula = prop;
				break;
			}
			/* Line 13 */
			outNode.getNewProperties().remove(newFormula);
			
			/* Line 15 */
			if (newFormula.isEvent() || 
					newFormula.getOperatorName().contentEquals("! ")) {
				if (checkNegation(newFormula, node.getOldProperties())) {
					return nodesSet;
				} else {
					// Reuse incoming node
					node.getNewProperties().remove(newFormula);
					node.getOldProperties().add(newFormula);
					return expand(node, outNodesSet);
				}
			/* Line 28 */
			} else if(newFormula.getOperatorName().contentEquals(" && ")) {
				/* Line 29-31 */
				Set<Operator> newProp = outNode.getNewProperties();
				newProp.add(newFormula.getAnd()[0]);
				newProp.add(newFormula.getAnd()[1]);
				
				newProp.removeAll(node.getOldProperties());
				outNode.getNewProperties().addAll(newProp);
				outNode.getOldProperties().add(newFormula);

				return expand(outNode, outNodesSet);
			/* Addon for X operator (section 3.4 by Gerth et al) */
			} else if (newFormula.getOperatorName().contentEquals("X ")) {
				outNode.getOldProperties().add(newFormula);
				outNode.getNextProperties().add(newFormula);
				return expand(outNode, outNodesSet);
			/* Line 20 */
			} else { /* U, R, || */
				/* Line 21-23 */
				TableauNode node1 = new TableauNode(outNode);
				node1.setName(newNodeName());
				Set<Operator> newSet1 = new1(
						newFormula.getSmartOperator()[0], 
	                    newFormula.getSmartOperator()[1], 
	                    newFormula.getOperatorName());
				newSet1.removeAll(node.getOldProperties());
				node1.getNewProperties().addAll(newSet1);
				node1.getOldProperties().add(newFormula);
				Set<Operator> nextSet = next1(
						newFormula.getSmartOperator()[0],
						newFormula.getSmartOperator()[1],
						newFormula.getOperatorName());
				node1.getNextProperties().addAll(nextSet);
				
				/* Line 24-26 */
				TableauNode node2 = new TableauNode(outNode);
				node2.setName(newNodeName());
				Set<Operator> newSet2 = new2(
						newFormula.getSmartOperator()[0], 
	                    newFormula.getSmartOperator()[1], 
	                    newFormula.getOperatorName());
				newSet2.removeAll(node.getOldProperties());
				node2.getNewProperties().addAll(newSet2);
				node2.getOldProperties().add(newFormula);
				
				/* Line 27 */
				return expand(node2, expand(node1, outNodesSet));
			}
		}
	}
	
	/**
	 * Returns a TableauNode iff there is already a node in nodeSet with the 
	 * same old and next fields as node. Line 5 of Gerth et al. algorithm.
	 */
	private TableauNode checkNodeSetExistence(TableauNode node, 
			Set<TableauNode> nodesSet) {
		Set<Operator> oldOps = node.getOldProperties();
		Set<Operator> nextOps = node.getNextProperties();
		
		for (TableauNode element: nodesSet) {
			boolean elementMightBeEqual = true;
			
			// Check if element.old == node.old
			for (Operator o1 : oldOps) {
				boolean elementEqual = false;
				
				for (Operator o2: element.getOldProperties()) {
					if (o1.equals(o2)) {
						elementEqual = true;
						break;
					}
				}
				
				// One of element.old should have matched by now
				if (!elementEqual) {
					elementMightBeEqual = false;
					break;
				}
			}
			
			// If the first condition fails for element, no need to check second
			if (!elementMightBeEqual) continue;
			
			// Check if element.next == node.next
			for (Operator o1 : nextOps) {
				boolean elementEqual = false;
				
				for (Operator o2: element.getNextProperties()) {
					if (o1.equals(o2)) {
						elementEqual = true;
						break;
					}
				}
				
				// One of element.old should have matched by now
				if (!elementEqual) {
					elementMightBeEqual = false;
					break;
				}
			}
			
			// Element is equal
			if (elementMightBeEqual) return element;
			
		}
		
		return null;

	}
	
	/**
	 * Checks if the negation of formula eta (proposition) is in old.
	 * Moreover, eta must be an event or negation of a proposition.
	 * Returns true iff the negation of proposition is in old.
	 * Line 16 in algorithm by Gerth et al.
	 */
	private boolean checkNegation(Operator proposition, Set<Operator> old) {
		if (proposition.isEvent()) {
			
			for(Operator oldElement: old) {
				if(!oldElement.getOperatorName().contentEquals("! ")) continue;
				
				if (proposition.getEvent().contentEquals(
						oldElement.getNot()[0].getEvent())) {
					return true;
				}
			}
			return false;
			
		} else if (proposition.getOperatorName().contentEquals("! ") &&
				proposition.getNot()[0].isEvent()) {
			
			for(Operator oldElement: old) {
				if (!oldElement.isEvent()) continue;
				
				if(proposition.getNot()[0].getEvent().contentEquals(
						oldElement.getEvent())) {
					return true;
				}
			}
			return false;
			
		} else {
			return false;
		}
	}
	
	private String newNodeName() {
		return "N" + ++nodeCounter;
	}
	
	private Set<Operator> new1(Operator a, Operator b, String operatorName) {
		if(operatorName.contentEquals(" U ")
				|| operatorName.contentEquals(" || ")) {
			Set<Operator> ret = new HashSet<>();
			ret.add(a);
			return ret;
		} else if (operatorName.contentEquals(" R ")) {
			Set<Operator> ret = new HashSet<>();
			ret.add(b);
			return ret;
		} else {
			return new HashSet<>();
		}
	}
	
	private Set<Operator> next1(Operator a, Operator b, String operatorName) {
		if(operatorName.contentEquals(" U ")) {
			Set<Operator> ret = new HashSet<>();
			Operator newOperator = new Operator();
			Operator[] operands = new Operator[2];
			operands[0] = a;
			operands[1] = b;
			newOperator.setUntil(operands);
			ret.add(newOperator);
			return ret;
		} else if (operatorName.contentEquals(" R ")) {
			Set<Operator> ret = new HashSet<>();
			Operator newOperator = new Operator();
			Operator[] operands = new Operator[2];
			operands[0] = a;
			operands[1] = b;
			newOperator.setRelease(operands);
			ret.add(newOperator);
			return ret;
		} else {
			return new HashSet<>();
		}
	}
	
	private Set<Operator> new2(Operator a, Operator b, String operatorName) {
		if(operatorName.contentEquals(" U ") || 
				operatorName.contentEquals(" || ")) {
			Set<Operator> ret = new HashSet<>();
			ret.add(b);
			return ret;
		} else if (operatorName.contentEquals(" R ")) {
			Set<Operator> ret = new HashSet<>();
			ret.add(a);
			ret.add(b);
			return ret;
		} else {
			return new HashSet<>();
		}
	}
	
}
