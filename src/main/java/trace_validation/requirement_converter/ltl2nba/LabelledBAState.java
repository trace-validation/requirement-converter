package trace_validation.requirement_converter.ltl2nba;

import java.util.*;

public class LabelledBAState extends BAState {
	
	private Set<Set<String>> labels;

	public LabelledBAState(String name) {
		super(name);
		
		labels = new HashSet<>();
	}

	public Set<Set<String>> getLabels() {
		return labels;
	}

	public void setLabels(Set<Set<String>> labels) {
		this.labels = labels;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		
		sb.append("Labels: ");
		for (Set<String> labelSet : labels) {
			for (String label : labelSet) sb.append(label + " \t");
			sb.append("\n");
		}
		sb.append("\n");
		
		return sb.toString();
	}
}
