package trace_validation.requirement_converter.ltl2nba;

import trace_validation.requirement_converter.interfaces.ltl.Operator;

/**
 * According to Gerth et al. the F, and G operators should be removed,
 * and negations pushed inside as far as possible.
 */
public class LtlSimplifier {
	public Operator simplify(Operator formula) {
		Operator newFormula = new Operator(formula);
		
		transform(newFormula, true, true, true, true);
		pushNegation(newFormula);
		return newFormula;
	}
	
	/**
	 * Transform formula to remove certain types of operators.
	 * Iff true, future and global operators are removed, respectively.
	 */
	private void transform(Operator formula, boolean future, boolean global, 
			boolean removeIf, boolean iff) {
		// Recurse
		if (formula.isOperator()) {
			if (formula.getNumberOfOperands() == 1) {
				transform(formula.getSmartOperator()[0], future, global, 
						removeIf, iff);
			} else {
				transform(formula.getSmartOperator()[0], future, global, 
						removeIf, iff);
				transform(formula.getSmartOperator()[1], future, global, 
						removeIf, iff);
			}
		}
		
		// Transform current operands
		String op = formula.getOperatorName();
		if (future && op.contentEquals("F ")) {
			/* (F a) == (true U a) */
			Operator[] until = new Operator[2];
			until[0] = new Operator();
			until[1] = new Operator();
			
			until[0].setEvent("true");
			
			until[1].setSmartOperator(formula.getFuture()[0]);		
			
			formula.setFuture(null);
			formula.setUntil(until);
		} else if (global && op.contentEquals("G ")) {
			/* (G a) == (! (true U (! a))) */
			Operator[] innerNot = new Operator[1];
			innerNot[0] = new Operator();
			Operator[] until = new Operator[2];
			until[0] = new Operator();
			until[1] = new Operator();
			Operator[] outerNot = new Operator[1];
			outerNot[0] = new Operator();
			
			innerNot[0].setSmartOperator(formula.getGlobally()[0]);
			
			until[0].setEvent("true");
			until[1].setNot(innerNot);
			
			outerNot[0].setUntil(until);

			formula.setGlobally(null);
			formula.setNot(outerNot);
		} else if (removeIf && op.contentEquals(" -> ")) {
			/* a -> b == !a || b */
			Operator[] or = new Operator[2];
			or[0] = new Operator();
			or[1] = new Operator();
			Operator[] not = new Operator[1];
			not[0] = new Operator();
			
			not[0].setSmartOperator(formula.getOperatorIf()[0]);
			or[0].setNot(not);
			or[1].setSmartOperator(formula.getOperatorIf()[1]);
			
			formula.setOperatorIf(null);
			formula.setOr(or);			
		} else if (iff && op.contentEquals(" <-> ")) {
			/* a <-> b == (!a || b) && (a || !b) */
			Operator[] or1 = new Operator[2];
			or1[0] = new Operator();
			or1[1] = new Operator();
			Operator[] not1 = new Operator[1];
			not1[0] = new Operator();
			Operator[] and = new Operator[2];
			and[0] = new Operator();
			and[1] = new Operator();
			Operator[] or2 = new Operator[2];
			or2[0] = new Operator();
			or2[1] = new Operator();
			Operator[] not2 = new Operator[1];
			not2[0] = new Operator();
			
			not1[0].setSmartOperator(formula.getIff()[0]);
			or1[0].setNot(not1);
			or1[1].setSmartOperator(formula.getIff()[1]);
			
			not2[0].setSmartOperator(formula.getIff()[1]);
			or2[0].setNot(not2);
			or2[1].setSmartOperator(formula.getIff()[0]);
			
			and[0].setOr(or1);
			and[1].setOr(or2);
			
			formula.setIff(null);
			formula.setAnd(and);
		} else {
			// Nothing to do in this situation
		}
	}
	
	/**
	 * Push negations inside until they are in front of events
	 */
	private void pushNegation(Operator formula) {
		// Nothing to simplify
		if (formula.isEvent()) return;
		if (formula.getOperatorName().contentEquals("! ") && 
				formula.getNot()[0].isEvent()) return;
		
		
		// Resolve negation
		if (formula.getOperatorName().contentEquals("! ")) {
			String opType = formula.getNot()[0].getOperatorName();
			Operator a = formula.getNot()[0].getSmartOperator()[0];
			Operator b = null;
			if (formula.getNot()[0].getNumberOfOperands() == 2) {
				b = formula.getNot()[0].getSmartOperator()[1];
			}
			
			// Important: clear the not operator of formula.
			// Do this beforehand since in !!a, a may also be a not.
			// Removing not would clear the result
			formula.setNot(null);
			
			// Skip F and G, these shouldn't be here
			
			// ! (X a) == X (! a)
			if (opType.contentEquals("X ")) {
				Operator[] not = new Operator[1];
				not[0] = new Operator();
				Operator[] next = new Operator[1];
				next[0] = new Operator();
				
				not[0].setSmartOperator(a);
				next[0].setNot(not);
				formula.setNext(next);
			}
			
			// !!a == a
			if (opType.contentEquals("! ")) {
				formula.setSmartOperator(a);
			}

			// ! (a || b) == !a && !b
			if (opType.contentEquals(" || ")) {
				Operator[] notA = new Operator[1];
				notA[0] = new Operator();
				Operator[] notB = new Operator[1];
				notB[0] = new Operator();
				Operator[] and = new Operator[2];
				and[0] = new Operator();
				and[1] = new Operator();
				
				notA[0].setSmartOperator(a);
				notB[0].setSmartOperator(b);
				and[0].setNot(notA);
				and[1].setNot(notB);
				formula.setAnd(and);
			}
			
			// ! (a && b) == !a || !b
			if (opType.contentEquals(" && ")) {
				Operator[] notA = new Operator[1];
				notA[0] = new Operator();
				Operator[] notB = new Operator[1];
				notB[0] = new Operator();
				Operator[] or = new Operator[2];
				or[0] = new Operator();
				or[1] = new Operator();
				
				notA[0].setSmartOperator(a);
				notB[0].setSmartOperator(b);
				or[0].setNot(notA);
				or[1].setNot(notB);
				formula.setOr(or);
			}
			
			// ! (a U b) == !a R !b
			if (opType.contentEquals(" U ")) {
				Operator[] notA = new Operator[1];
				notA[0] = new Operator();
				Operator[] notB = new Operator[1];
				notB[0] = new Operator();
				Operator[] release = new Operator[2];
				release[0] = new Operator();
				release[1] = new Operator();
				
				notA[0].setSmartOperator(a);
				notB[0].setSmartOperator(b);
				release[0].setNot(notA);
				release[1].setNot(notB);
				formula.setRelease(release);
			}
			
			// ! (a R b) == !a U !b
			if (opType.contentEquals(" R ")) {
				Operator[] notA = new Operator[1];
				notA[0] = new Operator();
				Operator[] notB = new Operator[1];
				notB[0] = new Operator();
				Operator[] until = new Operator[2];
				until[0] = new Operator();
				until[1] = new Operator();
				
				notA[0].setSmartOperator(a);
				notB[0].setSmartOperator(b);
				until[0].setNot(notA);
				until[1].setNot(notB);
				formula.setUntil(until);
			}
		}
		
		// Recurse
		if (formula.isEvent()) {
			// Nothing to simplify, may come from !!a where a is an event
			return;
		} else if (formula.getNumberOfOperands() == 1) {
			pushNegation(formula.getSmartOperator()[0]);
		} else {
			pushNegation(formula.getSmartOperator()[0]);
			pushNegation(formula.getSmartOperator()[1]);
		}
	}
}
