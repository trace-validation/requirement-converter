package trace_validation.requirement_converter.ltl2nba;

public class BAState {
	private String name;

	public BAState(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public boolean equals(Object o) {
    	if (o == this) return true;
    	if (o == null) return false;
    	if (! (o instanceof BAState)) return false;
    	
    	BAState state = (BAState) o;
    	return name.contentEquals(state.getName());
	}
	
	@Override
	public String toString() {
		return "Name: " + name + "\n";
	}
}
