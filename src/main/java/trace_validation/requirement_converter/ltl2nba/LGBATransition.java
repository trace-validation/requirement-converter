package trace_validation.requirement_converter.ltl2nba;

public class LGBATransition {

	LabelledBAState source;
	LabelledBAState destination;
	
	public LGBATransition(LabelledBAState src, LabelledBAState dst) {
		this.source = src;
		this.destination = dst;
	}

	public LabelledBAState getSrc() {
		return source;
	}

	public void setSrc(LabelledBAState source) {
		this.source = source;
	}

	public LabelledBAState getDst() {
		return destination;
	}

	public void setDst(LabelledBAState destination) {
		this.destination = destination;
	}
	
}
