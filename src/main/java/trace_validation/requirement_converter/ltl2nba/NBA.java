package trace_validation.requirement_converter.ltl2nba;

import java.util.*;

import trace_validation.requirement_converter.interfaces.ltl.Requirement;
import trace_validation.requirement_converter.interfaces.nba.*;

/** Non-deterministic B�chi automaton */
public class NBA {

	private Set<BAState> states 		= new HashSet<>();
	private Set<BAState> initial 		= new HashSet<>();
	private Set<GBATransition> transitions = new HashSet<>();
	private Set<BAState> accepting	= new HashSet<>();

	/** Construction of NBA from GBA by counter */
	public NBA(GBA gba) {
		// toArray() cannot be used here. In fact, arrays in general are
		// problematic, use a list instead.
		List<Set<BAState>> acceptingSets = new ArrayList<Set<BAState>>();
		for (Set<BAState> acceptingSet : gba.getAccepting()) {
		    acceptingSets.add(acceptingSet);
		}

		if (acceptingSets.size() == 0) {
			// NBA is mostly equal to GBA, with an empty set of accepting states
			// Normal construction would result in no states in the NBA
			this.states = gba.getStates();
			this.initial = gba.getInitial();
			this.transitions = gba.getTransitions();
			this.accepting.addAll(this.states);

//			System.out.println("No accepting sets, these are transitions:");
//			for (GBATransition t : this.transitions) {
//                System.out.println(t.getSrc().getName() + " -> \t" + t.getLbls() + " -> \t" + t.getDst().getName());
//            }
			return;
		}

		// Set states
		for (BAState state : gba.getStates()) {
			for (int i=0; i<acceptingSets.size(); ++i) {
//			    System.out.println("New state [" + state.getName() + "_" + i + "]" + i);
				BAState newState = new BAState(state.getName() + "_" + i);
				// Set state
				states.add(newState);
				// Set initial states
				if (i == 0 && gba.getInitial().contains(state)) {
                    initial.add(newState);
                }
			}
		}

		// Set accepting states
	    for (BAState state : acceptingSets.get(0)) {
			accepting.add(new BAState(state.getName() + "_0"));
		}

		// Set transitions
		for (GBATransition transition : gba.getTransitions()) {
			BAState src = transition.getSrc();
			BAState dst = transition.getDst();

			List<Integer> setIDs = isInAccepting(acceptingSets, transition.getSrc());

			// For all accepting sets in which the source is present
			for (int i=0; i<acceptingSets.size(); ++i) {
				// setIDs.contains(i)		(q,i) -> (q',i)
				// !setIDs.contains(i)		(q,i) -> (q',i+1)

				int dstID = setIDs.contains(i) ? i : (i+1) % acceptingSets.size();
				transitions.add(new GBATransition(
						new BAState(src.getName() + "_" + i),
						transition.getLbls(),
						new BAState(dst.getName() + "_" + dstID)));
			}
		}
	}
	
	public Set<BAState> getStates() {
		return states;
	}

	public void setStates(Set<BAState> states) {
		this.states = states;
	}

	public Set<BAState> getInitial() {
		return initial;
	}

	public void setInitial(Set<BAState> initial) {
		this.initial = initial;
	}

	public Set<GBATransition> getTransitions() {
		return transitions;
	}

	public void setTransitions(Set<GBATransition> transitions) {
		this.transitions = transitions;
	}

	public Set<BAState> getAccepting() {
		return accepting;
	}

	public void setAccepting(Set<BAState> accepting) {
		this.accepting = accepting;
	}

	/**
	 * Determines if state is in one of the accepting sets.
	 * Returns a list of all indices for which state is in the set, if it is in
	 * no sets, the list is empty.
	 */
	private List<Integer> isInAccepting(List<Set<BAState>> acceptingSets, BAState state) {
		List<Integer> result = new ArrayList<>();

		for (int i=0; i<acceptingSets.size(); ++i) {
			if (acceptingSets.get(i).contains(state)) {
                result.add(i);
            }
		}
		return result;
	}

	/**
	 * Convert internal NBA structure to Nba interface
	 */
	public Nba toNBAInterface(Requirement formula) {
		Nba output = new Nba();

		// Name
		output.setName(formula.getName());

		// Split
		if (formula.getSplit() != null) {
			output.setSplit(
					new ArrayList<String>(
							Arrays.asList(formula.getSplit())
							)
					);
		}

		// Where
		List<Where> whereList = new ArrayList<>();
		for (trace_validation.requirement_converter.interfaces.ltl.Where oldWhere :
				formula.getWhere()) {
			whereList.add(new Where(oldWhere));
		}
		output.setWhere(whereList);

		// Automaton
		Automaton automaton = new Automaton();
		// States
		List<State> automatonStates = new ArrayList<>();
		for (BAState state : this.states) {
			if (state.getName().contentEquals("init")) {
                continue;
            }

			State newState = new State();

			newState.setID(state.getName());
			if(this.initial.contains(state)) {
				newState.setInitial(true);
			} else {
				newState.setInitial(false);
			}
			if(this.accepting.contains(state)) {
				newState.setAccepting(true);
			} else {
				newState.setAccepting(false);
			}

			System.out.println("Adding " + newState.getID());

			automatonStates.add(newState);
		}
		automaton.setStates(automatonStates);

		// Transitions
		List<List<Transition>> automatonTransitions = new ArrayList<>();
		for (GBATransition transition :
				this.transitions) {
			if (transition.getSrc().getName().contentEquals("init")) {
                continue;
            }

			List<Transition> newTransition = new ArrayList<>();

			// Finish transition
			newTransition = toTransition(
					transition.getSrc().getName(),
					transition.getLbls(),
					transition.getDst().getName());
			automatonTransitions.add(newTransition);
		}
		automaton.setTransitions(automatonTransitions);

		output.setAutomaton(automaton);

		return output;
	}

	/**
	 * Transitions depend on the correct order of source, event and destination.
	 * This method constructs a transition according to those rules.
	 */
	private List<Transition> toTransition(String src, Set<String> events, String dst) {
		List<Transition> result = new ArrayList<>();
		Transition srcTrans = new Transition();
		Transition dstTrans = new Transition();
		Transition eventTrans = new Transition();

		srcTrans.stringValue = src;
		dstTrans.stringValue = dst;
		List<String> eventList = new ArrayList<>();
		for (String op : events) {
            eventList.add(op);
        }
		eventTrans.stringArrayValue = eventList;

		result.add(srcTrans);
		result.add(eventTrans);
		result.add(dstTrans);

		return result;
	}
}
