package trace_validation.requirement_converter.ltl2nba;

import java.util.*;

/** A LGBA with the labels pushed to incoming edges*/
public class GBA {

	private Set<BAState> states 		= new HashSet<>();
	private Set<BAState> initial 		= new HashSet<>();
	private Set<GBATransition> transitions = new HashSet<>();
	private Set<Set<BAState>> accepting	= new HashSet<>();
	
	public GBA(LGBA lgba) {
		this.states = new HashSet<>(lgba.getStates());
		this.initial = new HashSet<>(lgba.getInitial());
		
		for (Set<LabelledBAState> set : lgba.getAccepting()) {
			accepting.add(new HashSet<>(set));
		}
		
		// Push labels to incoming edges
		for (LGBATransition transition : lgba.getTransitions()) {
			for (Set<String> label : transition.getDst().getLabels()) {
				transitions.add(new GBATransition(
						transition.getSrc(), 
						label, 
						transition.getDst()));
			}
		}
	}

	public Set<BAState> getStates() {
		return states;
	}

	public void setStates(Set<BAState> states) {
		this.states = states;
	}

	public Set<BAState> getInitial() {
		return initial;
	}

	public void setInitial(Set<BAState> initial) {
		this.initial = initial;
	}

	public Set<GBATransition> getTransitions() {
		return transitions;
	}

	public void setTransitions(Set<GBATransition> transitions) {
		this.transitions = transitions;
	}

	public Set<Set<BAState>> getAccepting() {
		return accepting;
	}

	public void setAccepting(Set<Set<BAState>> accepting) {
		this.accepting = accepting;
	}
	
}
