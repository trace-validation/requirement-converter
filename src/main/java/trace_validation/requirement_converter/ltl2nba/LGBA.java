package trace_validation.requirement_converter.ltl2nba;

import trace_validation.requirement_converter.interfaces.ltl.Operator;

import java.util.*;

/**
 * Labelled generalised B�chi automaton
 */
public class LGBA {
	private Set<LabelledBAState> states 		= new HashSet<>();
	private Set<LabelledBAState> initial 		= new HashSet<>();
	private Set<LGBATransition> transitions 	= new HashSet<>();
	private Set<Set<LabelledBAState>> accepting	= new HashSet<>();
	
	public LGBA(Set<TableauNode> nodes, Operator formula) {
		/** Abstract propositions */
		Set<String> aps = formula.getAPs();
		
		for (TableauNode tn : nodes) {
			// Add state
			LabelledBAState newState = new LabelledBAState(tn.getName());
			states.add(newState);
			
			// Set labels
			Set<Set<String>> labels = new HashSet<>();
			Set<String> pos = new HashSet<>();
			Set<String> neg = new HashSet<>();
			
			// Fill pos and neg
			for (String ap : aps) {
				Operator negatedAP = new Operator();
				Operator apOperator = new Operator();
				apOperator.setEvent(ap);
				Operator[] operand = new Operator[]{apOperator};
				negatedAP.setNot(operand);
				
				for (Operator op : tn.getOldProperties()) {
					if (op.equals(apOperator)) pos.add(ap);
					if (op.equals(negatedAP)) neg.add(ap);
				}
			}
			
			// Create the powerset of APs, remove elements that do not contain
			// all elements of pos, or do contain any elements from neg.
			Set<Set<String>> powerSet = calculatePowerSet(aps);
			
			for (Set<String> set : powerSet) {
				boolean add = true;
				
				// Check if all pos are present
				for (String ap : pos) {
					if (!set.contains(ap)) add = false;
				}
				// Check if none neg are present
				for (String ap : neg) {
					if (set.contains(ap)) add = false;
				}
				
				if (add) labels.add(set);
			}
			newState.setLabels(labels);
			
			// Set initial states
			if (tn.getName().contentEquals("init")) initial.add(newState);
			for (TableauNode incoming : tn.getIncoming()) {
				if (incoming.getName().contentEquals("init")) {
					initial.add(newState);
				}
			}
			
		}
		
		// Set transitions
		for (TableauNode tn : nodes) {
			// Select corresponding dest state
			LabelledBAState dest = null;
			for (LabelledBAState candidateDest : states) {
				if (candidateDest.getName() == tn.getName()) {
					dest = candidateDest;
					break;
				}
			}
			
			for (TableauNode incoming : tn.getIncoming()) {
				// Select corresponding dest state
				LabelledBAState source = null;
				for (LabelledBAState candidateSource : states) {
					if (candidateSource.getName() == incoming.getName()) {
						source = candidateSource;
						break;
					}
				}
				
				// Add transition
				LGBATransition transition = new LGBATransition(source, dest);
				transitions.add(transition);
			}
		}
		
		// Set accepting states
		for (Operator union : extractUntil(formula)) {
			Operator b = union.getUntil()[1];
			
			Set<LabelledBAState> acceptStates = new HashSet<>();
			for (TableauNode tn : nodes) {
				if (!tn.getOldProperties().contains(union) ||
						tn.getOldProperties().contains(b)) {
					// Get corresponding LabelledBAState
					for (LabelledBAState state : states) {
						if (state.getName() == tn.getName()) {
							acceptStates.add(state);
							break;
						}
					}
				}
			}
			// Add this 'set of states' to the 'set of set of states'.
			accepting.add(acceptStates);
		}
		System.out.println(this);
	}
	
	public Set<LabelledBAState> getStates() {
		return states;
	}

	public Set<LabelledBAState> getInitial() {
		return initial;
	}

	public Set<LGBATransition> getTransitions() {
		return transitions;
	}

	public Set<Set<LabelledBAState>> getAccepting() {
		return accepting;
	}
	
	private Set<Operator> extractUntil(Operator formula) {
		Set<Operator> result = new HashSet<>();
		if (formula.isEvent()) return result;
		
		if (formula.getOperatorName().contentEquals(" U ")) result.add(formula);
		
		result.addAll(extractUntil(formula.getSmartOperator()[0]));
		if (formula.getNumberOfOperands() == 2) {
			result.addAll(extractUntil(formula.getSmartOperator()[1]));
		}
		
		return result;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		sb.append("\tInitial states:\n");
		for (LabelledBAState state : states) {
			if (initial.contains(state)) sb.append(state + "\n");
		}
		
		sb.append("\n\tOther states:\n");
		for (LabelledBAState state : states) {
			if (!initial.contains(state)) sb.append(state + "\n");
		}
		
		sb.append("\n\tTransitions:\n");
		for (LGBATransition transition : transitions) {
			sb.append(transition.getSrc().getName() + "\t\t->\t " + 
					transition.getDst().getName() + "\n");
		}
		
		sb.append("\n\tAccepting sets:\n");
		for (Set<LabelledBAState> acceptingSet : accepting) {
			for (LabelledBAState acceptingState : acceptingSet) {
				sb.append(acceptingState.getName() + "\t");
			}
			sb.append("\n");
		}
		
		return sb.toString();
	}
	
	private Set<Set<String>> calculatePowerSet(Set<String> set) {
		Set<Set<String>> powerSet = new HashSet<>();
		if (set.isEmpty()) {
			powerSet.add(new HashSet<String>());
			return powerSet;
		}
		// Extract one element from list
		String state = null;
		for (String selectedState : set) {
			state = selectedState;
			break;
		}
		Set<String> remainingSet = new HashSet<>(set);
		remainingSet.remove(state);
		
		// Add state to oldSet and add both versions of oldSet to powerSet
		for (Set<String> oldSet : calculatePowerSet(remainingSet)) {
			Set<String> newSet = new HashSet<>();
			newSet.add(state);
			newSet.addAll(oldSet);
			powerSet.add(newSet);
			powerSet.add(oldSet);
		}
		
		return powerSet;	
	}

}
