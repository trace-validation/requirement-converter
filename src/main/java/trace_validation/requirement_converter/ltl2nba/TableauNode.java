package trace_validation.requirement_converter.ltl2nba;

import java.util.*;

import trace_validation.requirement_converter.interfaces.ltl.Operator;

/**
 * Nodes of a tableau according to Gerth et al.
 */
public class TableauNode {
	private String name;
	private Set<TableauNode> incoming;
	private Set<Operator> newProperties;
	private Set<Operator> oldProperties;
	private Set<Operator> nextProperties;
	
	/**
	 * Create empty node
	 */
	public TableauNode() {
		name = "";
		incoming = 		new HashSet<>();
		newProperties = new HashSet<>();
		oldProperties = new HashSet<>();
		nextProperties = new HashSet<>();
	}
	
	public TableauNode(TableauNode tn) {
		this.name = 			tn.name;
		this.incoming = 		new HashSet<>(tn.getIncoming());
		this.newProperties = 	new HashSet<>(tn.getNewProperties());
		this.oldProperties = 	new HashSet<>(tn.getOldProperties());
		this.nextProperties = 	new HashSet<>(tn.getNextProperties());
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = new String(name);
	}

	public Set<TableauNode> getIncoming() {
		return incoming;
	}

	public void setIncoming(Set<TableauNode> incoming) {
		this.incoming = new HashSet<TableauNode>(incoming);
	}

	public Set<Operator> getNewProperties() {
		return newProperties;
	}

	public void setNewProperties(Set<Operator> newProperties) {
		this.newProperties = new HashSet<Operator>(newProperties);
	}

	public Set<Operator> getOldProperties() {
		return oldProperties;
	}

	public void setOldProperties(Set<Operator> oldProperties) {
		this.oldProperties = new HashSet<Operator>(oldProperties);
	}

	public Set<Operator> getNextProperties() {
		return nextProperties;
	}

	public void setNextProperties(Set<Operator> nextProperties) {
		this.nextProperties = new HashSet<Operator>(nextProperties);
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Name:\t\t " + name);
		sb.append("\nIncoming:\t ");
		for (TableauNode node : incoming) sb.append(node.name + "#\t ");
		sb.append("\nNew:\t ");
		for (Operator op : newProperties) sb.append(op + "#\t ");
		sb.append("\nOld:\t ");
		for (Operator op: oldProperties) sb.append(op + "#\t ");
		sb.append("\nNext:\t ");
		for (Operator op: nextProperties) sb.append(op + "#\t ");
		sb.append("\n\n\n");
		return sb.toString();
	}
	
	@Override
	public boolean equals(Object o) {
    	if (o == this) return true;
    	if (o == null) return false;
    	if (! (o instanceof TableauNode)) return false;
    	
    	TableauNode node = (TableauNode) o;
    	
    	return node.getName().contentEquals(name);
	}
}
