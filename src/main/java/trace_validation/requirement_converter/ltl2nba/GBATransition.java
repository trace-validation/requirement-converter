package trace_validation.requirement_converter.ltl2nba;

import java.util.*;

public class GBATransition {

	private BAState source;
	private Set<String> labels;
	private BAState destination;
	
	public GBATransition(BAState src, Set<String> lbls, BAState dst) {
		this.source = src;
		this.labels = lbls;
		this.destination = dst;
	}

	public BAState getSrc() {
		return source;
	}

	public void setSrc(BAState source) {
		this.source = source;
	}

	public Set<String> getLbls() {
		return labels;
	}

	public void setLbls(Set<String> labels) {
		this.labels = labels;
	}

	public BAState getDst() {
		return destination;
	}

	public void setDst(BAState destination) {
		this.destination = destination;
	}
	
}
