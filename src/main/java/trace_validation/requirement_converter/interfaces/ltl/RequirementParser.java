/**
 * Generated through https://app.quicktype.io, with JSON Schema at
 * src/main/resources/ltl3.schema.json.
 * 
 * Use as follows:
 * import trace_validation.requirement_converter.interfaces.ltl3.Converter;
 * LTL3 data = Converter.fromJsonString(jsonString);
 */

package trace_validation.requirement_converter.interfaces.ltl;


import java.io.IOException;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.core.JsonProcessingException;

public class RequirementParser {
    // Serialize/deserialize helpers

    public static Requirement fromJsonString(String json) throws IOException {
        return getObjectReader().readValue(json);
    }

    public static String toJsonString(Requirement obj) throws JsonProcessingException {
        return getObjectWriter().writeValueAsString(obj);
    }

    private static ObjectReader reader;
    private static ObjectWriter writer;

    private static void instantiateMapper() {
        ObjectMapper mapper = new ObjectMapper();
        reader = mapper.readerFor(Requirement.class);
        writer = mapper.writerFor(Requirement.class);
    }

    private static ObjectReader getObjectReader() {
        if (reader == null) instantiateMapper();
        return reader;
    }

    private static ObjectWriter getObjectWriter() {
        if (writer == null) instantiateMapper();
        return writer;
    }
}
