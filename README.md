# Requirement converter
This project contains a Java tool to convert an LTL formula to an NBA data structure. This conversion is based on the paper by Bauer et al. [1]

## Conversion steps
The conversion is done in a series of steps, each briefly described below.

### LTL specification
The program takes a JSON specification, as described in [ltl3.schema.json](./src/main/resources/ltl3.schema.json). The parser is a modified version of [quicktype](https://app.quicktype.io/) and is located in the [interfaces.ltl3](./src/main/java/trace_validation/requirement_converter/interfaces/ltl3) Java package. A positive and negative version of this formula are created.

### LTL simplification
According to the algorithm by Gerth et al. [2], the LTL formula should be simplified. This is done in the [LtlSimplifier](./src/main/java/trace_validation/requirement_converter/ltl2nba/LtlSimplifier.java) class.

### Tableau construction
Following the simplification, the algorithm by Gerth et al. [2] prescribes to construct a so-called tableau. The LTL formula is converted step-by-step to nodes in the tableau. The [TableauCreator](./src/main/java/trace_validation/requirement_converter/ltl2nba/TableauCreator.java) class performs the conversion, the [TableauNode](./src/main/java/trace_validation/requirement_converter/ltl2nba/TableauNode.java) stores the result.

### LGBA
From the Tableau, it is relatively simple to construct a labelled generalised B�chi automaton. This is also described by Gerth et al. [2]. The construction is performed in the constructor of the [LGBA](./src/main/java/trace_validation/requirement_converter/ltl2nba/LGBA.java) class.

### GBA
To convert the LGBA to a GBA is trivial. The labels in the nodes have to be pushed to the incoming transitions. This is done in the constructor of [GBA](./src/main/java/trace_validation/requirement_converter/ltl2nba/GBA.java).

### NBA
The conversion of GBA to NBA (non-deterministic B�chi automaton) is less straight forward. The basis of the algorithm comes from Tsay [3], with the addition of a special case if there are no accepting sets of states in the GBA. In that case, the GBA can be converted to an NBA with an empty accepting set. The conversion is done in the constructor of the [NBA](./src/main/java/trace_validation/requirement_converter/ltl2nba/NBA.java) class.

## Requirements
* JDK (developed for `8.0`)
* Apache Maven
* JSON specification of a requirement

## Usage
1. `git clone https://gitlab.com/trace-validation/requirement-converter`
2. `cd requirement-converter`
3. `mvn install`
4. `cd target`
5. `java -jar requirement-converter-0.0.1-SNAPSHOT-jar-with-dependencies.jar <Input Requirement> <Output Büchi> <Negated Output Büchi>`

### Example
This project comes with two example requirements, `ltl3_simple.json` and `ltl_example.json` where the latter is more complex. Run these as follows in step 5 above:

`java -jar requirement-converter-0.0.1-SNAPSHOT-jar-with-dependencies.jar ../src/main/resources/ltl3_simple.json ../src/main/resources/ltl3_simple.nba.json ../src/main/resources/neg_ltl3_simple.nba.json`

`java -jar requirement-converter-0.0.1-SNAPSHOT-jar-with-dependencies.jar ../src/main/resources/ltl_example.json ../src/main/resources/ltl_example.nba.json ../src/main/resources/neg_ltl_example.nba.json`

## References
[1] Andreas Bauer, Martin Leucker and Christian Schallhart. _Monitoring of Real-Time Properties._ 2006.

[2] R. Gerth, D. Peled, M.Y. Vardi and P. Wolper. _Simple On-the-fly Automatic Verification of Linear Temporal Logic._ 1996.

[3] Yin-Kuen Tsay. _B�chi Automata and Model Checking._ 2009.
